/**
 *
 */
package br.com.mobilesys.model;

/**
 * Enum para indicar o Status Ativo de um registro.
 * 
 * @author Geraldo Vieira - 29/10/2012
 * 
 */
public enum StatusAtivo {

	/**
	 * Indica que o registro esta ativo.
	 */
	ATIVO(0) {
		@Override
		public String getDescricao() {
			return "Ativo";
		}

	},
	/**
	 * Indica que o registro esta inativo.
	 */
	INATIVO(1) {
		@Override
		public String getDescricao() {
			return "Inativo";
		}

	};
	
	private int valor;
	public abstract String getDescricao();

	private StatusAtivo(int status) {
		this.valor = status;
	}


}
