package br.com.mobilesys.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name = "email", length = 40)
	private String email;

	@Column(name = "imei", length = 20, unique = true)
	private String imei;

	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "telefone", nullable = false, length = 15)
	private String telefone;

	@Column(name = "online", nullable = false)
	private Boolean online = Boolean.FALSE;

	@Column(name = "bateria_primeira", nullable = true)
	private Integer bateriaPrimeira;

	@Column(name = "bateria", nullable = true)
	private Integer bateria;

	@Column(name = "fotos_restantes", nullable = false)
	private Integer fotosRestantes = 0;

	@Column(name = "versao_totalcross", nullable = true)
	private String versaoTotalCross;

	@Column(name = "versao_litebase", nullable = true)
	private String versaoLitebase;

	@Column(name = "versao_promoter", nullable = true)
	private String versaoPromoter = "1.0";

	@Column(name = "data_versao", nullable = true)
	private String dataVersao;

	@Column(name = "ip", nullable = false, length = 15)
	private String ip = "";

	@Column(name = "ultimo_acesso", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar ultimoAcesso;

	@Column(name = "primeiro_acesso", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar primeiroAcesso;

	@Column(name = "comando", nullable = false, length = 15)
	private String comando = "";

	@Column(name = "equipe", nullable = false)
	private String equipe = "";

	@Column(name = "apps_instalados")
	private String appsInstalados;

	@Column(name = "mb_mes", nullable = false)
	private double mbMes = 0.0;

	@Enumerated
	@Column(name = "enviar_base", nullable = false)
	private SimNao enviarBase = SimNao.NAO;

	@Enumerated
	@Column(name = "atualizar", nullable = false)
	private SimNao atualizar = SimNao.NAO;

	@Enumerated
	@Column(name = "gsp_ativo", nullable = false)
	private SimNao gpsAtivo = SimNao.NAO;

	// bi-directional many-to-one association to Supervisor
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_supervisor", referencedColumnName = "id", nullable = false)
//	private Supervisor supervisor;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "status", length = 40)
	private StatusAtivo statusAtivo = StatusAtivo.ATIVO;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "vendedor")
	private SimNao vendedor = SimNao.NAO;

	@Column(name = "tipo_conexao")
	private String tipoConexao;

	@Column(name = "id_push")
	private String idPush;

	@Column(name = "bairro")
	private String bairro;

	@Column(name = "cep", length = 8)
	private String cep;

	@Column(name = "cpf")
	private String cpf;

	@Column(name = "pis")
	private String pis;

	@Column(name = "endereco")
	private String endereco;

	@Column(name = "latitude", length = 20)
	private Double latitude;

	@Column(name = "longitude", length = 20)
	private Double longitude;

	@Column(name = "fotos_reenvio")
	private String fotosReenvio;

	@Column(name = "salario")
	private Double salario;

	@Column(name = "encargos")
	private Double encargos;
	
	
	
	public static void main(String[] args) {
		for (SimNao string : SimNao.values()) {
			System.out.println(string.getDescricao());
		}
		for (StatusAtivo string : StatusAtivo.values()) {
			System.out.println(string.getDescricao());
		}
	}
	
	
}
