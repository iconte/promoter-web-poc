package br.com.mobilesys.model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Geraldo Vieira
 * 
 */
@Entity
@Table(name = "pergunta")
@ApiModel
public class Pergunta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, updatable = false)
	private Long id;

	@Size(min = 2, max = 255, message = "A descrição deve ter entre 2 e 255 caracteres.")
	@NotNull(message="O campo descrição deve ser preenchido.")
	@Column(name = "descricao", length = 255, nullable = false)
	private String descricao;

	@NotNull(message="O campo tipo campo resposta deve ser preenchido.")
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "tipo_campo_resposta", nullable = false)
	private TipoCampoResposta tipoCampoResposta;

	@Column(name = "obrigatorio")
	private SimNao obrigatorio = SimNao.SIM;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "ativo")
	private SimNao ativo = SimNao.SIM;

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "foto")
	private SimNao foto = SimNao.NAO;

	@Column(name = "mascara")
	private String mascara;

	public Pergunta() {
	}

	public Pergunta(Long id) {
		this.id = id;
	}
	
	@ApiModelProperty(position = 1, required = false, value = "id da pergunta",example="1")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ApiModelProperty(position = 2, required = true, value = "descrição da pergunta.",example="Qual a capital do PR?")
	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	@ApiModelProperty(position = 3, required = true, value = "tipo de resposta",example="2",name="tipoCampoResposta")
	public TipoCampoResposta getTipoCampoResposta() {
		return tipoCampoResposta;
	}

	public void setTipoCampoResposta(TipoCampoResposta tipoCampoResposta) {
		this.tipoCampoResposta = tipoCampoResposta;
	}
	@ApiModelProperty(position = 4, required = false, value = "define se a pergunta é obrigatória.",example="0")
	public SimNao getObrigatorio() {
		return obrigatorio;
	}

	public void setObrigatorio(SimNao obrigatorio) {
		this.obrigatorio = obrigatorio;
	}

	@ApiModelProperty(position = 5, required = false, value = "ativa/inativa a pergunta.",example="0")
	public SimNao getAtivo() {
		return ativo;
	}

	public void setAtivo(SimNao ativo) {
		this.ativo = ativo;
	}
	@ApiModelProperty(position = 6, required = false, value = "habilita/desabilita foto.",example="1")
	public SimNao getFoto() {
		return foto;
	}

	public void setFoto(SimNao foto) {
		this.foto = foto;
	}
	@ApiModelProperty(position = 7, required = false, value = "mascara",example="mascara")
	public String getMascara() {
		return mascara;
	}

	public void setMascara(String mascara) {
		this.mascara = mascara;
	}
	
	public enum TipoCampoResposta {
		INTEGER("Número Inteiro"),
		DOUBLE("Número Fracionário"),
		STRING("Alfanumérico"),
		CHECKBOX("Múltipla Escolha"),
		RADIOBUTTON("Única Escolha"),
		DATE("Data"),
		DATA_TIME("Data com Hora"),
		TIME("Hora"),
		TRUE_FALSE("Sim/Não"),
		EMAIL("E-mail"),
		CIDADE("Cidade");

		private final String descricao;

		private TipoCampoResposta(String descricao) {
			this.descricao = descricao;
		}

		public String getDescricao() {
			return descricao;
		}

		@Override
		public String toString() {
			return descricao;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Pergunta))
			return false;
		Pergunta other = (Pergunta) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Pergunta [id=").append(id).append(", descricao=")
				.append(descricao).append(", tipoCampoResposta=")
				.append(tipoCampoResposta).append(", obrigatorio=")
				.append(obrigatorio).append(", ativo=").append(ativo)
				.append(", foto=").append(foto).append(", mascara=")
				.append(mascara).append("]");
		return builder.toString();
	}
	
	
	
}
