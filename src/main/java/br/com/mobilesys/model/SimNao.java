/**
 * 
 */
package br.com.mobilesys.model;

/**
 * Utilizado para identificar se um atributo de uma {@link Entidade}.
 * 
 * @author Geraldo - 24/04/2012
 * 
 */
public enum SimNao {

	/**
	 * Não.
	 */
	NAO("Não"),
	/*
	 * Sim
	 */
	SIM("Sim");

	private final String descricao;

	private SimNao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Obtem o valor de descricao.
	 * 
	 * @return O valor de descricao.
	 */
	public String getDescricao() {
		return descricao;
	}

	@Override
	public String toString() {
		return descricao;
	}
}
