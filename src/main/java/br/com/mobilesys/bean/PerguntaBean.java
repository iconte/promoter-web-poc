package br.com.mobilesys.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.MessagingException;

import org.primefaces.context.RequestContext;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.lazy.PerguntaLazy;
import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;
import br.com.mobilesys.model.SimNao;
import br.com.mobilesys.service.EmailService;
import br.com.mobilesys.service.PerguntaService;
import br.com.mobilesys.util.FacesUtil;

@Named
@ViewScoped
public class PerguntaBean extends AbstractExportBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@Inject
	private PerguntaService perguntaService;
	@Inject
	private PerguntaLazy perguntaLazy;
//	@Inject
	private EmailService emailService;
	

	private List<Pergunta> perguntas = new ArrayList<>();
	private Pergunta pergunta = new Pergunta();

	public PerguntaBean() {
	}

	
	public void novo() throws MessagingException {
		this.pergunta = new Pergunta();
		exibirDialogCadastro();
	}

	private void exibirDialogCadastro() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgCadastro').show()");
	}

	public void editar() {
		exibirDialogCadastro();
	}

	public void salvar() {
		try {
			perguntaService.salvar(pergunta);
			exibirMensagemSucesso();
//			List<String> lista = new ArrayList<>();
//			lista.add("ivconte@gmail.com");
//			emailService.enviarEmail(lista, String.format("pergunta \" %s \" salva com sucesso.", pergunta.getDescricao()),
//					"pergunta salva com sucesso!", "");
		} catch (PromoterException e) {
			exibirMensagemErro(e);
		}

	}
	
	/*public void exportarExcel() throws IOException{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		ServletContext servletContext = (ServletContext) externalContext.getContext();
		HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
		String pathRel;
		try {
			perguntas = perguntaService.listarPerguntas();
			pathRel = servletContext.getRealPath("/WEB-INF/relatorios/relatorio_pergunta.jasper");
			ByteArrayOutputStream output = (ByteArrayOutputStream)exportService.exportarExcel(perguntas, pathRel);
			response.setContentType("application/vnd.ms-excel");
			response.addHeader("Content-Disposition", "attachment;filename=relatorio_pergunta.xls");
			response.setContentLength(output.size());
			response.getOutputStream().write(output.toByteArray(), 0, output.size());
			response.getOutputStream().flush();
			response.getOutputStream().close();
			facesContext.responseComplete();
			facesContext.renderResponse();
		} catch (PromoterException e) {
			exibirMensagemErro(e);
		}
	}*/
	
	public void exportarExcel() throws IOException{
		try {
			List<Pergunta> lista = perguntaService.listarPerguntas();
			setNomeArquivoSaida("relatorio_pergunta.xls");
			setNomeRelatorio("relatorio_pergunta.jasper");
			setLista(lista);
			processarRelatorio();
		} catch (PromoterException e) {
			FacesUtil.addErrorMessage(e.getMessage());
		}
		
	}

	private void exibirMensagemSucesso() {
		FacesUtil.addSuccessMessage("Adicionado com sucesso.");
	}

	private void exibirMensagemErro(PromoterException e) {
		RequestContext.getCurrentInstance().addCallbackParam("manterModal", true);
		FacesUtil.addErrorMessage(e.getMessage());
	}

	public void excluir() throws PromoterException {
		perguntaService.excluir(pergunta);
	}

	public List<Pergunta> getPerguntas() {
		return perguntas;
	}

	public void setPerguntas(List<Pergunta> perguntas) {
		this.perguntas = perguntas;
	}

	public Pergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}

	public TipoCampoResposta[] getTiposPergunta() {
		return TipoCampoResposta.values();
	}

	public SimNao[] getSimNao() {
		SimNao[] values = SimNao.values();
		Collections.sort(Arrays.asList(values), Collections.reverseOrder(Comparator.comparing(SimNao::ordinal)));
		return values;
	}

	public PerguntaLazy getPerguntaLazy() {
		return perguntaLazy;
	}

}
