package br.com.mobilesys.bean;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.service.ExportService;

public abstract class AbstractExportBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ExportService exportService;
	
	private List<?> lista;
	private String nomeRelatorio;
	private String nomeArquivoSaida;
	private final String PATH = "/WEB-INF/relatorios/";

	public void processarRelatorio() throws IOException, PromoterException {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		ServletContext servletContext = (ServletContext) externalContext
				.getContext();
		HttpServletResponse response = (HttpServletResponse) externalContext
				.getResponse();
		String pathRel;
		try {
			pathRel = servletContext
					.getRealPath(PATH + nomeRelatorio);
			ByteArrayOutputStream output = (ByteArrayOutputStream) exportService
					.exportarExcel(lista, pathRel);
			response.setContentType("application/vnd.ms-excel");
			response.addHeader("Content-Disposition",
					String.format("attachment;filename=%s", nomeArquivoSaida));
			response.setContentLength(output.size());
			response.getOutputStream().write(output.toByteArray(), 0,
					output.size());
			response.getOutputStream().flush();
			response.getOutputStream().close();
			facesContext.responseComplete();
			facesContext.renderResponse();
		} catch (Exception e) {
			throw new PromoterException(e.getMessage());
		}
	}

	public List<?> getLista() {
		return lista;
	}

	public void setLista(List<?> lista) {
		this.lista = lista;
	}

	public String getNomeRelatorio() {
		return nomeRelatorio;
	}

	public void setNomeRelatorio(String nomeRelatorio) {
		this.nomeRelatorio = nomeRelatorio;
	}

	public String getNomeArquivoSaida() {
		return nomeArquivoSaida;
	}

	public void setNomeArquivoSaida(String nomeArquivoSaida) {
		this.nomeArquivoSaida = nomeArquivoSaida;
	}
	
}
