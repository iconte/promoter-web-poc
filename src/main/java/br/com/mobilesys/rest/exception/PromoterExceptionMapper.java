package br.com.mobilesys.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import br.com.mobilesys.exception.PromoterException;

public class PromoterExceptionMapper implements ExceptionMapper<br.com.mobilesys.exception.PromoterException>{

	@Override
	public Response toResponse(PromoterException exception) {
		  return Response.status(Status.BAD_REQUEST).
			      entity(exception.getCause().getMessage()).
			      type("text/plain").
			      build();
	}
}
