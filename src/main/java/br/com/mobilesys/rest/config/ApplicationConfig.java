package br.com.mobilesys.rest.config;

import io.swagger.jaxrs.config.BeanConfig;

import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import br.com.mobilesys.service.impl.PerguntaServiceImpl;

@ApplicationPath("/rest")
public class ApplicationConfig extends Application {

	private static final String RESOURCE_PACKAGE = PerguntaServiceImpl.class
			.getPackage().getName();

	public ApplicationConfig() {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setTitle("Promoter Rest API");
		beanConfig.setDescription("Descrição dos serviços REST oferecidos pelo Promoter.");
		beanConfig.setVersion("1.0.2");
		beanConfig.setSchemes(new String[] { "http" });
		beanConfig.setHost("localhost:8080"); // ex. "localhost:8002"
		beanConfig.setBasePath("/promoter-web-dev/rest"); // ex.
																			// "/api"
		beanConfig.setPrettyPrint(true);

		beanConfig.setResourcePackage(RESOURCE_PACKAGE); // ex.
															// "io.swagger.resources"
		beanConfig.setScan(true);
		 
		
	}

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> resources = new java.util.HashSet<>();
		resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
		resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
		addRestResourceClasses(resources);
		return resources;
	}

	/**
	 * Do not modify addRestResourceClasses() method. It is automatically
	 * populated with all resources defined in the project. If required, comment
	 * out calling this method in getClasses().
	 */
	private void addRestResourceClasses(Set<Class<?>> resources) {
		resources
				.add(br.com.mobilesys.rest.PerguntaRestController.class);
	}

}