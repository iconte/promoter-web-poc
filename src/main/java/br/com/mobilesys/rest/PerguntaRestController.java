package br.com.mobilesys.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.primefaces.json.JSONObject;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;
import br.com.mobilesys.model.SimNao;
import br.com.mobilesys.service.PerguntaService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Path("/pergunta")
@Api(value = "/pergunta")
public class PerguntaRestController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	PerguntaService perguntaService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(produces="application/json",httpMethod="GET", value = "lista perguntas")
	@ApiResponses(value={@ApiResponse(code=204,message="no content"), @ApiResponse(code=200,message="ok"),@ApiResponse(code=200,message="internal error")})
	public Response listarPerguntas() {
		try {
			Gson gson = new Gson();
			Type type = new TypeToken<List<Pergunta>>() {
			}.getType();
			List<Pergunta> perguntas = perguntaService.listarPerguntas();

			if (perguntas != null && !perguntas.isEmpty()) {
				String json = gson.toJson(perguntas, type);
				return Response.ok(json).build();
			} else {
				return Response.noContent().build();
			}
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(produces="application/json",httpMethod="GET", value = "obter por id")
	@ApiResponses(value={@ApiResponse(code=204,message="no content"), @ApiResponse(code=200,message="ok"),@ApiResponse(code=200,message="internal error")})
	@ApiParam(name="id",required=true,allowableValues="numeros inteiros",value="aceita numeros inteiros")
	public Response obterPorId(@PathParam("id") Long id) {
		try {
			Gson gson = new Gson();
			Pergunta pergunta = perguntaService.obterPorId(id);
			if (pergunta != null) {
				return Response.ok(gson.toJson(pergunta)).build();
			} else {
				return Response.noContent().build();
			}
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	@ApiOperation(produces="application/json",httpMethod="DELETE", value = "excluir")
	@ApiResponses(value={@ApiResponse(code=400,message="bad request"), @ApiResponse(code=200,message="ok"),@ApiResponse(code=200,message="internal error")})
	@ApiParam(name="id",required=true,allowableValues="numeros inteiros")
	public Response excluir(@PathParam("id") Long id) {
		try {
			Pergunta pergunta = perguntaService.obterPorId(id);
			if (pergunta == null) {
				return Response.status(Status.BAD_REQUEST).build();
			}
			perguntaService.excluir(pergunta);
			return Response.ok().build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(produces="application/json",httpMethod="POST", value = "salvar")
	@ApiResponses(value={@ApiResponse(code=412,message="pre condition failed"), @ApiResponse(code=201,message="created"),@ApiResponse(code=200,message="internal error")})
	@ApiParam(name="pergunta",required=true,allowableValues="json contendo a pergunta a ser incluida")
	public Response salvar(String  perguntaJSON)  {
		try {
			JSONObject jsonObject = new JSONObject(perguntaJSON);
			Pergunta pergunta = new Pergunta();
//			pergunta.setAtivo(SimNao.valueOf(jsonObject.getString("ativo")));
//			pergunta.setDescricao(jsonObject.getString("ativo"));
//			pergunta.setFoto(SimNao.valueOf(jsonObject.getString("foto")));
//			pergunta.setMascara(jsonObject.getString("mascara"));
//			pergunta.setTipoCampoResposta(TipoCampoResposta.valueOf(jsonObject.getString("tipoCampoResposta")));
			perguntaService.salvar(pergunta);
			return Response.status(Status.CREATED).build();
		} catch (PromoterException e) {
			return Response.status(Status.PRECONDITION_FAILED)
					.entity(e.getMessage()).build();
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(produces="application/json",httpMethod="PUT", value = "atualizar")
	@ApiResponses(value={@ApiResponse(code=400,message="pre condition failed"), @ApiResponse(code=201,message="created"),@ApiResponse(code=200,message="internal error")})
	@ApiParam(name="pergunta",required=true,allowableValues="json contendo a pergunta a ser atualizada.")
	public Response atualizar(String perguntaJSON) {
		try {
			Gson gson = new Gson();
			Pergunta pergunta = gson.fromJson(perguntaJSON, Pergunta.class);
			if (pergunta == null || pergunta.getId()==null) {
				return Response.status(Status.BAD_REQUEST).build();
			} else {
				boolean existe = perguntaService.obterPorId(pergunta.getId()) != null;
				if (existe) {
					perguntaService.salvar(pergunta);
				} else {
					return Response.status(Status.BAD_REQUEST).entity(pergunta)
							.build();
				}
				return Response.status(Status.CREATED).build();
			}
		} catch (Exception e) {
			return Response.serverError().build();
		}
	}

}
