package br.com.mobilesys.banco;

import org.eclipse.persistence.config.DescriptorCustomizer;
import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.history.HistoryPolicy;

public class PerguntaHistoryCustomizer implements DescriptorCustomizer {

	@Override
	public void customize(ClassDescriptor descriptor) throws Exception {
		  HistoryPolicy policy = new HistoryPolicy();
	        policy.addHistoryTableName("pergunta","pergunta_hist");
	        policy.addStartFieldName("start_date");
	        policy.addEndFieldName("end_date");
	        policy.setShouldHandleWrites(true);
	        descriptor.setHistoryPolicy(policy);
	}
	
}
