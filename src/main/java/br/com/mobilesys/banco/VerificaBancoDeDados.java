/**
 * 
 */
package br.com.mobilesys.banco;

import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.sessions.Session;

/**
 * @author Geraldo Vieira - 29/01/2016
 *
 */
public class VerificaBancoDeDados implements SessionCustomizer {
	 
	public void customize(Session session) throws Exception {
		session.getIntegrityChecker().checkDatabase();	
		session.getIntegrityChecker().setShouldCatchExceptions(false);
	}
}
