package br.com.mobilesys.service;

import java.net.MalformedURLException;
import java.util.List;

import javax.mail.MessagingException;

public interface EmailService {
	
	public boolean hostEstaNoAr() throws MalformedURLException ;
	
	public void enviarEmail(List<String> emails, String assunto, String corpoMensagem, String caminhoArquivo) throws MessagingException;
	
	

}
