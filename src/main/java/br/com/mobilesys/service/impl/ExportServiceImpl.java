package br.com.mobilesys.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.service.ExportService;

public class ExportServiceImpl implements ExportService, Serializable{


	private static final long serialVersionUID = 8008300712709195259L;
	@Override
	public OutputStream exportarExcel(List<?> lista,String caminhoJasper) throws PromoterException{
		JRSwapFileVirtualizer virtualizer = null;
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put(JRParameter.REPORT_LOCALE, new Locale("pt", "BR"));
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
		    JRSwapFile swapFile = new JRSwapFile(Paths.get("C:\\dump").toString(), 1024, 100);
		    virtualizer = new JRSwapFileVirtualizer(50, swapFile, true);
		    parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			JasperPrint print = JasperFillManager.fillReport(caminhoJasper, parameters,ds);
			JRXlsExporter exporterXLS = new JRXlsExporter();
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			exporterXLS.setConfiguration(obterConfiguracaoExcel());
			exporterXLS.setExporterInput(new SimpleExporterInput(print));
			exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(output));
			exporterXLS.exportReport();
			return output;
		} catch (Exception e) {
			throw new PromoterException(e.getMessage());
		}finally {
		    if (virtualizer != null) virtualizer.cleanup();
		}
		
	}
	
	private SimpleXlsReportConfiguration obterConfiguracaoExcel() {
		SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
		configuration.setOnePagePerSheet(true);
		configuration.setDetectCellType(true);
		configuration.setWhitePageBackground(true);
		configuration.setRemoveEmptySpaceBetweenColumns(true);
		configuration.setRemoveEmptySpaceBetweenRows(true);
		configuration.setDetectCellType(true);
		return configuration;
	}

}
