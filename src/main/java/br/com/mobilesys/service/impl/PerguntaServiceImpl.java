package br.com.mobilesys.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;
import br.com.mobilesys.repository.PerguntaRepository;
import br.com.mobilesys.service.PerguntaService;

public class PerguntaServiceImpl implements PerguntaService, Serializable {

	private static final long serialVersionUID = 7286683176145200614L;
	@Inject
	private PerguntaRepository perguntaRepository;
	

	@Override
	public List<Pergunta> listarPerguntas() {
		return perguntaRepository.findAll();
	}

	@Override
	public List<Pergunta> listarPerguntasPorTipo(TipoCampoResposta tipoResposta) {
		return perguntaRepository
				.findOptionalByTipoCampoRespostaEqual(tipoResposta);
	}

	
	@Override
	public void salvar(Pergunta pergunta) throws PromoterException {
		try {
			Pergunta old = null;
			if (pergunta != null) {
				old = obterPorDescricao(pergunta.getDescricao());
				boolean temDescricaoIgualNoBanco = old != null;

				if (temDescricaoIgualNoBanco) {
					if(pergunta.getId()==null || pergunta.getId() != null && !old.getId().equals(pergunta.getId()))
					{
						throw new PromoterException(
								String.format(
										"Já existe uma pergunta cadastrada com a descrição %s.",
										pergunta.getDescricao()));
					}else{
						perguntaRepository.save(pergunta);
					}
				} else {
					perguntaRepository.save(pergunta);
				}
			} else {
				throw new PromoterException("Informe a pergunta.");
			}
		} catch (Exception e) {
			throw new PromoterException(e);
		}

	}

	@Override
	public void excluir(Pergunta pergunta) throws PromoterException {
		try {
			perguntaRepository.remove(pergunta);
		} catch (Exception e) {
			throw new PromoterException(e);
		}

	}

	@Override
	public Pergunta obterPorId(Long id) throws PromoterException {
			return perguntaRepository.findBy(id);
	}

	@Override
	public Pergunta obterPorDescricao(String descricao)
			throws PromoterException {
		try {
			return perguntaRepository.obterPorDescricao(descricao);
		} catch (Exception e) {
			throw new PromoterException(e);
		}
	}

}
