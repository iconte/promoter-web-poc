package br.com.mobilesys.service;

import java.io.OutputStream;
import java.util.List;

import br.com.mobilesys.exception.PromoterException;

public interface ExportService {
	public OutputStream exportarExcel(List<?> lista,String caminhoJasper) throws PromoterException;
}
