package br.com.mobilesys.service;

import java.util.List;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;

public interface PerguntaService {

	public List<Pergunta> listarPerguntas();

	public List<Pergunta> listarPerguntasPorTipo(TipoCampoResposta tipoResposta);

	public void salvar(Pergunta pergunta) throws PromoterException;

	public void excluir(Pergunta pergunta) throws PromoterException;

	public Pergunta obterPorId(Long id) throws PromoterException;
	
	public Pergunta obterPorDescricao(String descricao) throws PromoterException;
}
