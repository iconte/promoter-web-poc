package br.com.mobilesys.test.service;

import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.testcontrol.api.TestControl;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;
import br.com.mobilesys.service.ExportService;

@RunWith(CdiTestRunner.class)
@TestControl(projectStage = ProjectStage.UnitTest.class)
public class ExportServiceTest {

	@Inject
	private ExportService exportService;

	@Test
	public void deveRetornarOutputStream() throws PromoterException, URISyntaxException {
		List<Pergunta> perguntas = obterListaPerguntas();
		URL resource = this.getClass().getResource("/jasper/relatorio_pergunta.jasper");
		String caminho = Paths.get(resource.toURI()).toString();
		ByteArrayOutputStream output = (ByteArrayOutputStream) exportService
				.exportarExcel(perguntas, caminho);
		assertNotNull(output);
		
		/**
		 * geração arquivo teste local
		 */
		/*Path path = java.nio.file.Paths.get("C:\\teste\\relatorio_teste.xls");
		try (FileOutputStream fileOutputStream = new FileOutputStream(path.toString())) {
			fileOutputStream.write(output.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		}*/
	
	}
	
	@Test(expected=PromoterException.class)
	public void deveLancarException() throws PromoterException {
		exportService.exportarExcel(null,null);
	}

	private List<Pergunta> obterListaPerguntas() {
		List<Pergunta> list = new ArrayList<>();
		Pergunta pergunta = new Pergunta();
		Pergunta pergunta2 = new Pergunta();
		pergunta.setId(1L);
		pergunta.setDescricao("p1 teste");
		pergunta.setTipoCampoResposta(TipoCampoResposta.CIDADE);
		pergunta2.setId(2L);
		pergunta2.setDescricao("p2 teste");
		pergunta2.setTipoCampoResposta(TipoCampoResposta.CHECKBOX);
		list.add(pergunta);
		list.add(pergunta2);
		return list;
	}
}
