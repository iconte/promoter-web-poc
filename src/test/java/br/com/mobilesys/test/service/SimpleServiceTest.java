package br.com.mobilesys.test.service;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.testcontrol.api.TestControl;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.mobilesys.service.SimpleService;

@RunWith(CdiTestRunner.class)
@TestControl(projectStage = ProjectStage.UnitTest.class)
public class SimpleServiceTest {

	@Inject
	private SimpleService simpleService;

	@Test
	public void deveRetornarVerdadeiro() {
		String retonaHello = simpleService.retonaHello();
		assertEquals("Hello", retonaHello);
	}

}
