package br.com.mobilesys.test.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import javax.inject.Inject;

import org.apache.deltaspike.core.api.projectstage.ProjectStage;
import org.apache.deltaspike.testcontrol.api.TestControl;
import org.apache.deltaspike.testcontrol.api.junit.CdiTestRunner;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.mobilesys.exception.PromoterException;
import br.com.mobilesys.model.Pergunta;
import br.com.mobilesys.model.Pergunta.TipoCampoResposta;
import br.com.mobilesys.service.PerguntaService;

import com.github.dbunit.rules.cdi.api.UsingDataSet;

@RunWith(CdiTestRunner.class)
@TestControl(projectStage = ProjectStage.UnitTest.class)
public class PerguntaServiceTest {

	@Inject
	private PerguntaService perguntaService;
	
	
	@Test
	@UsingDataSet("pergunta.yml")
	public void deveRetornarLista() {
		List<Pergunta> listarPerguntas = perguntaService.listarPerguntas();
		Assert.assertEquals(3, listarPerguntas.size());
	}
	
	@Test
	@UsingDataSet("pergunta.yml")
	public void deveRetornarListaCheckbox() {
		List<Pergunta> listarPerguntas = perguntaService.listarPerguntasPorTipo(TipoCampoResposta.CHECKBOX);
		Assert.assertEquals(1, listarPerguntas.size());
	}
	
	@Test(expected = PromoterException.class)
	@UsingDataSet("pergunta.yml")
	public void deveRetornarPromoterException() throws PromoterException {
		Pergunta pergunta = null;
		perguntaService.salvar(pergunta);
	}
	
	@Test(expected = PromoterException.class)
	@UsingDataSet("pergunta.yml")
	public void perguntaRepetidaDeveRetornarPromoterException() throws PromoterException {
		Pergunta pergunta = new Pergunta();
		pergunta.setDescricao("pergunta2");
		perguntaService.salvar(pergunta);
	}
	@Test(expected = PromoterException.class)
	@UsingDataSet("pergunta.yml")
	public void perguntaRepetidaIdDiferenteDeveRetornarPromoterException() throws PromoterException {
		Pergunta pergunta = new Pergunta();
		pergunta.setId(999L);
		pergunta.setDescricao("pergunta2");
		perguntaService.salvar(pergunta);
	}
	
	@Test(expected = PromoterException.class)
	@UsingDataSet("pergunta.yml")
	public void perguntaSemDescricaoDeveRetornarPromoterException() throws PromoterException {
		Pergunta pergunta = new Pergunta();
		perguntaService.salvar(pergunta);
	}
	
	@Test(expected = PromoterException.class)
	@UsingDataSet("pergunta.yml")
	public void removerPerguntaVaziaDeveRetornarPromoterException() throws PromoterException {
		Pergunta pergunta = null;
		perguntaService.excluir(pergunta);
	}
	
	@Test
	@UsingDataSet("pergunta.yml")
	public void deveExcluirPergunta() throws PromoterException {
		Pergunta pergunta = perguntaService.obterPorDescricao("pergunta1");
		assertThat(pergunta).isNotNull();
		assertThat(pergunta.getDescricao()).isEqualTo("pergunta1");
		perguntaService.excluir(pergunta);
		pergunta = perguntaService.obterPorDescricao("pergunta1");
		assertThat(pergunta).isNull();
	}
	
	@Test
	@UsingDataSet("pergunta.yml")
	public void deveAtualizarPergunta() throws PromoterException {
		Pergunta pergunta = perguntaService.obterPorId(1L);
		assertThat(pergunta).isNotNull();
		assertThat(pergunta.getDescricao()).isEqualTo("pergunta1");
		pergunta.setDescricao("Pergunta updated2");
		perguntaService.salvar(pergunta);
		pergunta = perguntaService.obterPorId(1L);
		assertThat(pergunta.getDescricao()).isEqualTo("Pergunta updated2");
	}

	@Test
	@UsingDataSet("pergunta.yml")
	public void deveAcharPergunta() throws PromoterException {
		Pergunta pergunta = perguntaService.obterPorId(2L);
		assertThat(pergunta.getDescricao()).isNotNull().isEqualTo("pergunta2");
	}
}
