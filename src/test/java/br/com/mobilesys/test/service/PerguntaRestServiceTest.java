package br.com.mobilesys.test.service;

import org.junit.Rule;
import org.junit.runner.RunWith;

import com.eclipsesource.restfuse.Assert;
import com.eclipsesource.restfuse.Destination;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import com.eclipsesource.restfuse.Method;
import com.eclipsesource.restfuse.Response;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

@RunWith( HttpJUnitRunner.class )
public class PerguntaRestServiceTest {
	 @Rule
	  public Destination restfuse = new Destination( this, "http://localhost:8080/promoter-web-dev/rest" );
	  
	  @Context
	  private Response response;
	  
	  @HttpTest( method = Method.GET, path = "/pergunta" ) 
	  public void checkRestfuseOnlineStatus() {
	    Assert.assertNoContent(response );
	  }
	
}
